//
//  OSBBasketViewController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBBasketViewController.h"

#import "OSBBasketManager.h"
#import "OSBBasketItemCell.h"
#import "OSBItem.h"

@interface OSBBasketViewController () {
	NSArray *items;
}

@end

@implementation OSBBasketViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self updateView:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateView:) name:OSBBasketManagerItemsChangedNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[self setEditing:NO];
}

- (void)updateView:(NSNotification *)notification
{
	items = [[OSBBasketManager defaultManager] items];
	
	NSNumber *itemIndex = [[notification userInfo] objectForKey:@"itemIndex"];
	if (itemIndex) {
		[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[itemIndex unsignedIntValue] inSection:0]]
													   withRowAnimation:UITableViewRowAnimationFade];
	} else {
		[tableView reloadData];
	}
	
	float totalSum = [[OSBBasketManager defaultManager] totalSum];
	if (totalSum) {
		[orderButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Order (Ls %1.2f)", nil), totalSum]
					 forState:UIControlStateNormal];		
		orderButton.enabled = YES;
	} else {
		orderButton.enabled = NO;
	}
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	[tableView setEditing:editing animated:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"BasketItem";
    OSBBasketItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if (!cell) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"OSBBasketItemCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];		
	}
	
	cell.itemIndex = indexPath.row;
	OSBItem *item = [items objectAtIndex:indexPath.row];
	
	NSString *name = item.name ? item.name : item.barcode;
	[(UILabel *)[cell viewWithTag:1] setText:name];
	[(UILabel *)[cell viewWithTag:2] setText:[NSString stringWithFormat:@"Ls %1.2f", item.price]];
	[(UILabel *)[cell viewWithTag:3] setText:[NSString stringWithFormat:@"%d", item.amount]];
    
    return cell;
}

- (IBAction)makeOrderTapped:(id)sender
{
	[self.navigationController pushViewController:thankYouViewController animated:YES];
	[[OSBBasketManager defaultManager] makeOrder];
}

- (IBAction)returnToHomeScreenTapped:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		[[OSBBasketManager defaultManager] removeItemAtIndex:indexPath.row];
		items = [[OSBBasketManager defaultManager] items];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		[self updateView:nil];
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
