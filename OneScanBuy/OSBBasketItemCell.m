//
//  OSBBasketItemCell.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBBasketItemCell.h"
#import "OSBBasketManager.h"

@implementation OSBBasketItemCell

@synthesize itemIndex;

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[(UIButton *)[self viewWithTag:4] addTarget:self action:@selector(decreaseItemAmount:) forControlEvents:UIControlEventTouchUpInside];
		[(UIButton *)[self viewWithTag:5] addTarget:self action:@selector(increaseItemAmount:) forControlEvents:UIControlEventTouchUpInside];
		
		self.contentView.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
	}
	return self;
}

- (void)increaseItemAmount:(id)sender
{
	[[OSBBasketManager defaultManager] increaseAmountForItemAtIndex:itemIndex];
}

- (void)decreaseItemAmount:(id)sender
{
	[[OSBBasketManager defaultManager] decreaseAmountForItemAtIndex:itemIndex];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	
	[self viewWithTag:3].hidden = editing;
	[self viewWithTag:4].hidden = editing;
	[self viewWithTag:5].hidden = editing;
	
	CGRect frame = [[self viewWithTag:1] frame];
	frame.size.width = editing ? 270.0f : 185.0f;
	[[self viewWithTag:1] setFrame:frame];
}

@end
