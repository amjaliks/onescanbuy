//
//  OSBProductManager.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBProductManager.h"
#import "OSBItem.h"
#import "OSBBasketManager.h"
#import "CJSONDeserializer.h"

NSString * const OSBProductManagerFavoriteItemsUpdatedNotification = @"OSBProductManagerFavoriteItemsUpdatedNotification";

@interface OSBProductManager () {
	NSArray *favoriteItems;
	NSArray *dealItems;
}
 
@end

@implementation OSBProductManager

+ (OSBProductManager *)defaultManager
{
	static OSBProductManager *instance;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[OSBProductManager alloc] init];
	});
	
	return instance;
}

- (id)init
{
	self = [super init];
	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadFavoriteItems) name:OSBBasketManagerOrderSentNotification object:nil];
		
		[self loadFavoriteItems];
		[self loadDealItems];
	}
	return self;
}

- (NSArray *)favoriteItems
{
	if (!favoriteItems) {
		[self loadFavoriteItems];
	}
	
	return favoriteItems;
}

- (NSArray *)dealItems
{
	return dealItems;
}

- (void)loadFavoriteItems
{
	NSString *user = [[NSUserDefaults standardUserDefaults] stringForKey:@"user"];
	NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://91.224.13.92/oneScanBuy/user/%@/favoriteProducts", user]];

	[self loadItemsFromURL:URL completion:^(NSArray * items) {
		favoriteItems = items;
		[[NSNotificationCenter defaultCenter] postNotificationName:OSBProductManagerFavoriteItemsUpdatedNotification object:self];
	}];
}

- (void)loadDealItems
{
	NSURL *URL = [NSURL URLWithString:@"http://91.224.13.92/oneScanBuy/deals"];
	
	[self loadItemsFromURL:URL completion:^(NSArray * items) {
		dealItems = items;
	}];
}

- (void)loadItemsFromURL:(NSURL *)URL completion:(void (^)(NSArray *items))completion
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSURLRequest *request = [NSURLRequest requestWithURL:URL];
		NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
		
		if (data && [data length]) {
			NSArray *itemInfos = [[CJSONDeserializer deserializer] deserializeAsArray:data error:nil];
			NSMutableArray *items = [NSMutableArray arrayWithCapacity:[itemInfos count]];
			
			for (NSDictionary *itemInfo in itemInfos) {
				OSBItem *item = [[OSBItem alloc] initWithBarcode:[itemInfo objectForKey:@"barCode"]];
				item.name = [itemInfo objectForKey:@"name"];
				item.price = [[itemInfo objectForKey:@"price"] floatValue];
				id oldPrice = [itemInfo objectForKey:@"oldPrice"];
				item.oldPrice = [oldPrice respondsToSelector:@selector(floatValue)] ? [oldPrice floatValue] : 0.0f;
				
				[items addObject:item];
			}				
			
			dispatch_async(dispatch_get_main_queue(), ^{
				completion(items);
			});
		}
	});
}

@end
