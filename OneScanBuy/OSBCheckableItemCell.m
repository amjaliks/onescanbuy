//
//  OSBBasketItemCell.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBCheckableItemCell.h"
#import "OSBBasketManager.h"

@interface OSBCheckableItemCell () {
	UIButton *button;
	UILabel *price;
	UILabel *oldPrice;
	UIView *strike;
}
@end

@implementation OSBCheckableItemCell

@synthesize item;

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		button = (UIButton *)[self viewWithTag:3];
		[button addTarget:self action:@selector(checkBoxTapped:) forControlEvents:UIControlEventTouchUpInside];
		
		price = (UILabel *)[self viewWithTag:2];
		oldPrice = (UILabel *)[self viewWithTag:4];
		strike = (UILabel *)[self viewWithTag:5];
		
		self.contentView.backgroundColor = [UIColor colorWithWhite:1.0f alpha:0.3f];
	}
	return self;
}

- (void)setItem:(OSBItem *)newItem
{
	item = newItem;
	[(UILabel *)[self viewWithTag:1] setText:item.name];
	[price setText:[NSString stringWithFormat:@"Ls %1.2f", item.price]];
	[button setSelected:[[OSBBasketManager defaultManager] isItemWithBarcodeInBasket:item.barcode]]; 
	
	if (item.oldPrice) {
		price.textColor = [UIColor redColor];
		
		oldPrice.hidden = NO;
		oldPrice.text = [NSString stringWithFormat:@"Ls %1.2f", item.oldPrice];
		
		CGSize size = [oldPrice.text sizeWithFont:oldPrice.font];
		CGRect frame = strike.frame;
		frame.size.width = size.width;
		strike.frame = frame;
		strike.hidden = NO;
	} else {
		price.textColor = [UIColor blackColor];
		oldPrice.hidden = YES;
		strike.hidden = YES;
	}
}

- (void)checkBoxTapped:(id)sender
{
	if (button.selected) {
		button.selected = NO;
		[[OSBBasketManager defaultManager] removeItemWithBarcode:item.barcode];
	} else {
		button.selected = YES;
		[[OSBBasketManager defaultManager] addItemWithBarcode:item.barcode];
	}
}

@end
