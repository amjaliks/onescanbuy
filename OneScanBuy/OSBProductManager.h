//
//  OSBProductManager.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const OSBProductManagerFavoriteItemsUpdatedNotification;

@interface OSBProductManager : NSObject

+ (OSBProductManager *)defaultManager;

- (NSArray *)favoriteItems;
- (NSArray *)dealItems;

@end
