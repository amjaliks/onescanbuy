//
//  OSBRootViewController.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIViewController;

@interface OSBRootViewController : UIViewController {
	IBOutlet UIViewController *quickScanViewController;
	IBOutlet UIViewController *productsViewController;
	IBOutlet UIViewController *basketViewController;
	
	IBOutlet UIImageView *scrollingImageView;
}

@end
