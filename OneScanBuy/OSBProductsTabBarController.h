//
//  OSBProductsTabBarControllerViewController.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSBProductsTabBarController : UITabBarController <UITabBarControllerDelegate>

@end
