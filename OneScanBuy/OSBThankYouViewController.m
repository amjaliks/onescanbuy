//
//  OSBThankYouViewController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.29.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBThankYouViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface OSBThankYouViewController () {
	CADisplayLink *displayLink;
}

@end

@implementation OSBThankYouViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(scrollBottom:)];
	[displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[displayLink invalidate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)scrollBottom:(id)sender
{
	CGRect frame = scrollingImageView.frame;
	frame.origin.x -= 0.5f;
	if (frame.origin.x < - 794.0f) frame.origin.x += 794.0f;
	scrollingImageView.frame = frame;
}


@end
