//
//  OSBAppDelegate.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OSBRootViewController;

@interface OSBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) OSBRootViewController *viewController;

@end
