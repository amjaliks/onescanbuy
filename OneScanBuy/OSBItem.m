//
//  OSBItem.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBItem.h"

@implementation OSBItem

@synthesize amount;
@synthesize barcode;
@synthesize name;
@synthesize oldPrice;
@synthesize price;
@synthesize resolved;

- (id)initWithBarcode:(NSString *)newBarcode
{
	self = [self init];
	if (self) {
		barcode = newBarcode;
		amount = 1;
		resolved = NO;
	}
	return self;
}

@end
