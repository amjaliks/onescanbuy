//
//  OSBItem.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OSBItem : NSObject

@property (nonatomic, strong) NSString *barcode;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) float price;
@property (nonatomic) float oldPrice;
@property (nonatomic) NSUInteger amount;
@property (nonatomic) BOOL resolved;

- (id)initWithBarcode:(NSString *)barcode;

@end
