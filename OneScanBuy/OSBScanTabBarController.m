//
//  OSBScanTabBarController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBScanTabBarController.h"

#import "OSBBasketManager.h"

@interface OSBScanTabBarController ()

@end

@implementation OSBScanTabBarController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self updateBasketBadge];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBasketBadge) name:OSBBasketManagerItemCountChangedNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)updateBasketBadge
{
	UITabBarItem *basketTabBar = [self.tabBar.items objectAtIndex:1];
	
	NSUInteger itemCount = [[OSBBasketManager defaultManager] itemCount];
	basketTabBar.badgeValue = itemCount ? [NSString stringWithFormat:@"%d", itemCount] : nil;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
	self.navigationItem.rightBarButtonItem = viewController.navigationItem.rightBarButtonItem;
}

@end
