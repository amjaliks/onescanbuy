//
//  OSBRootViewController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBRootViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface OSBRootViewController () {
	CADisplayLink *displayLink;
}

@end

@implementation OSBRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TopLogo"]];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	CGRect frame = scrollingImageView.frame;
	frame.origin.x = 0;
	scrollingImageView.frame = frame;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(scrollBottom:)];
	[displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];	
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[displayLink invalidate];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (void)scrollBottom:(id)sender
{
	CGRect frame = scrollingImageView.frame;
	frame.origin.x -= 0.5f;
	if (frame.origin.x < - 794.0f) frame.origin.x += 794.0f;
	scrollingImageView.frame = frame;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)quickScanTapped:(id)sender
{
	[self.navigationController pushViewController:quickScanViewController animated:YES];
}

- (IBAction)productsTapped:(id)sender
{
	[self.navigationController pushViewController:productsViewController animated:YES];
}

- (IBAction)basketTapped:(id)sender
{
	[self.navigationController pushViewController:basketViewController animated:YES];
}

@end
