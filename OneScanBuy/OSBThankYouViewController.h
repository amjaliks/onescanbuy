//
//  OSBThankYouViewController.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.29.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSBThankYouViewController : UIViewController {
	IBOutlet UIImageView *scrollingImageView;
}

@end
