//
//  OSBScanViewController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBScanViewController.h"

#import <AudioToolbox/AudioToolbox.h>
#import "OSBBasketManager.h"
#import "ZBarSDK.h"

@interface OSBScanViewController () {
	SystemSoundID beepSoundID;
}

@end

@implementation OSBScanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	ZBarImageScanner *scanner = readerView.scanner;
	[scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
	[scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];
	[scanner setSymbology:ZBAR_EAN13 config:ZBAR_CFG_ENABLE to:1];
	
	readerView.readerDelegate = self;
	readerView.zoom = 1.0f;
	
	NSURL *beepURL = [[NSBundle mainBundle] URLForResource:@"beep" withExtension:@"mp3"];
	AudioServicesCreateSystemSoundID((__bridge CFURLRef) beepURL, &beepSoundID);	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
	
	AudioServicesDisposeSystemSoundID(beepSoundID);
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		[readerView start];
	});
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	[readerView stop];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)readerView:(ZBarReaderView *)rv didReadSymbols:(ZBarSymbolSet *)symbols fromImage:(UIImage *)image
{
	for (ZBarSymbol *symbol in symbols) {
		
		BOOL valid = NO;
		NSString *data = symbol.data;
		if (symbol.type == ZBAR_EAN13) {
			valid = symbol.quality > 20;
		} else if (symbol.type == ZBAR_QRCODE) {
			NSString *eanRegex = @"[0-9]{1,13}"; 
			NSPredicate *eanRegexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", eanRegex];
			
			valid = [eanRegexTest evaluateWithObject:data];
		}
		
		if (valid) {
			AudioServicesPlayAlertSound(beepSoundID);
			[[OSBBasketManager defaultManager] addItemWithBarcode:symbol.data];
			
			UIImageView *imageView = [[UIImageView alloc] initWithFrame:readerView.frame];
			imageView.contentMode = UIViewContentModeScaleAspectFill;
			imageView.clipsToBounds = YES;
			imageView.image = image;
			[self.tabBarController.view addSubview:imageView];
			
			[UIView animateWithDuration:0.5f
							 animations:^{
								 imageView.alpha = 0.0f;
								 imageView.frame = CGRectMake(240.0f, 387.0f, 1.0f, 1.0f);
							 } completion:^(BOOL finished) {
								 [imageView removeFromSuperview];
							 }];
			
		} else {
			[readerView flushCache];
		}
	}
}

@end
