//
//  OSBBasketManager.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const OSBBasketManagerItemsChangedNotification;
extern NSString * const OSBBasketManagerItemCountChangedNotification;
extern NSString * const OSBBasketManagerOrderSentNotification;

@interface OSBBasketManager : NSObject

@property (readonly) NSUInteger itemCount;
@property (readonly) NSArray *items;
@property (readonly) float totalSum;

+ (OSBBasketManager *)defaultManager;

- (void)addItemWithBarcode:(NSString *)barcode;
- (void)removeItemWithBarcode:(NSString *)barcode;
- (void)removeItemAtIndex:(NSUInteger)index;
- (BOOL)isItemWithBarcodeInBasket:(NSString *)barcode;
- (void)increaseAmountForItemAtIndex:(NSUInteger)index;
- (void)decreaseAmountForItemAtIndex:(NSUInteger)index;
- (void)makeOrder;

@end
