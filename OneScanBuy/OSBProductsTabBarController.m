//
//  OSBProductsTabBarControllerViewController.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBProductsTabBarController.h"
#import "OSBBasketManager.h"

@interface OSBProductsTabBarController ()

@end

@implementation OSBProductsTabBarController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.delegate = self;
	self.selectedIndex = 2;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self updateBasketBadge];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBasketBadge) name:OSBBasketManagerItemCountChangedNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)updateBasketBadge
{
	UITabBarItem *basketTabBar = [self.tabBar.items objectAtIndex:4];
	
	NSUInteger itemCount = [[OSBBasketManager defaultManager] itemCount];
	basketTabBar.badgeValue = itemCount ? [NSString stringWithFormat:@"%d", itemCount] : nil;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
	self.navigationItem.rightBarButtonItem = viewController.navigationItem.rightBarButtonItem;
}

@end
