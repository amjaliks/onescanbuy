//
//  OSBBasketItemCell.h
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSBBasketItemCell : UITableViewCell

@property (nonatomic) NSInteger itemIndex;

@end
