//
//  OSBBasketManager.m
//  OneScanBuy
//
//  Created by Aleksejs Mjaliks on 12.04.28.
//  Copyright (c) 2012. g. A25 SIA. All rights reserved.
//

#import "OSBBasketManager.h"

#import "OSBItem.h"
#import "CJSONDeserializer.h"
#import "CJSONSerializer.h"

NSString * const OSBBasketManagerItemsChangedNotification = @"OSBasketManagerItemsChangedNotification";
NSString * const OSBBasketManagerItemCountChangedNotification = @"OSBasketManagerItemCountChangedNotification";
NSString * const OSBBasketManagerOrderSentNotification = @"OSBBasketManagerOrderSentNotification";

@interface OSBBasketManager () {
	NSMutableArray *items;
}

@end

@implementation OSBBasketManager

+ (OSBBasketManager *)defaultManager
{
	static OSBBasketManager *instance;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[OSBBasketManager alloc] init];
	});
	
	return instance;
}

- (id)init
{
	self = [super init];
	if (self) {
		items = [NSMutableArray array];
	}
	return self;
}

- (NSUInteger)itemCount
{
	NSUInteger count = 0;
	for (OSBItem *item in items) {
		if (item.amount) count++;
	}
	return count;
}

- (NSArray *)items
{
	return [items copy];
}

- (float)totalSum
{
	float sum = 0.0f;
	for (OSBItem *item in items) {
		sum += item.amount * item.price;
	}

	return sum;
}

- (BOOL)isItemWithBarcodeInBasket:(NSString *)barcode
{
	OSBItem *item = [[items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"barcode == %@", barcode]] lastObject];
	
	return item.amount;
}

- (void)addItemWithBarcode:(NSString *)barcode
{
	OSBItem *item = [[items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"barcode == %@", barcode]] lastObject];
	
	if (item) {
		[self increaseAmountForItemAtIndex:[items indexOfObject:item]];
	} else {
		item = [[OSBItem alloc] initWithBarcode:barcode];
		[items addObject:item];
	
		[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemCountChangedNotification object:self];
		
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSURL *URL = [NSURL URLWithString:[@"http://91.224.13.92/oneScanBuy/product/" stringByAppendingString:item.barcode]];
			NSURLRequest *request = [NSURLRequest requestWithURL:URL];
			NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
			
			if (data && [data length]) {
				NSDictionary *itemInfo = [[CJSONDeserializer deserializer] deserializeAsDictionary:data error:nil];
				item.name = [itemInfo objectForKey:@"name"];
				item.price = [[itemInfo objectForKey:@"price"] floatValue];
				id oldPrice = [itemInfo objectForKey:@"oldPrice"];
				item.oldPrice = [oldPrice respondsToSelector:@selector(floatValue)] ? [oldPrice floatValue] : 0.0f;
				item.resolved = YES;
				
				dispatch_async(dispatch_get_main_queue(), ^{
					NSUInteger index = [items indexOfObject:item];
					[self postNotificationAboutItemAtIndex:index];
				});
			}
		});
	}
}

- (void)removeItemWithBarcode:(NSString *)barcode
{
	OSBItem *item = [[items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"barcode == %@", barcode]] lastObject];
	if (item) {
		[items removeObject:item];
		[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemCountChangedNotification object:self];
	}
}

- (void)removeItemAtIndex:(NSUInteger)index
{
	[items removeObjectAtIndex:index];
	[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemCountChangedNotification object:self];
}

- (void)increaseAmountForItemAtIndex:(NSUInteger)index
{
	OSBItem *item = [items objectAtIndex:index];
	item.amount++;
	[self postNotificationAboutItemAtIndex:index];
}

- (void)decreaseAmountForItemAtIndex:(NSUInteger)index
{
	OSBItem *item = [items objectAtIndex:index];
	if (item.amount) {
		item.amount--;
		[self postNotificationAboutItemAtIndex:index];
		
		if (!item.amount) {
			[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemCountChangedNotification object:self];
		}
	}
}

- (void)makeOrder
{
	NSArray *copy = items;
	items = [NSMutableArray array];
	[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemCountChangedNotification object:self];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSMutableArray *orderItems = [NSMutableArray arrayWithCapacity:[items count]];
		for (OSBItem *item in copy) {
			if (item.resolved && item.amount) {
				[orderItems addObject:[NSDictionary dictionaryWithObjectsAndKeys:
									   item.barcode, @"barCode",
									   [NSNumber numberWithUnsignedInteger:item.amount], @"quantity", nil]];
			}
		}
		
		NSDictionary *order = [NSDictionary dictionaryWithObject:orderItems forKey:@"items"];
		NSString *user = [[NSUserDefaults standardUserDefaults] stringForKey:@"user"];
		
		NSData *data = [[CJSONSerializer serializer] serializeDictionary:order error:nil];
		NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://91.224.13.92/oneScanBuy/user/%@/order", user]];
		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:data];
		
		[NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
		
		[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerOrderSentNotification object:nil];
	});
}

- (void)postNotificationAboutItemAtIndex:(NSUInteger)index
{
	[[NSNotificationCenter defaultCenter] postNotificationName:OSBBasketManagerItemsChangedNotification
														object:self
													  userInfo:[NSDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInteger:index]
																						   forKey:@"itemIndex"]];	
}

@end
